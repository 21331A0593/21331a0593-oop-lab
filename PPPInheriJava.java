
class Inheritance{
    public int pub=1;
    protected int  prot=2;
    private int priv=3;
    public int callpri(){
        return priv;
    }
}

class Child extends Inheritance{
    int callpro(){
        return prot;
    }
}
public class PPPInheriJava{
    public static void main(String[] args){
         Child obj= new Child();
         System.out.println("Value declared in public : "+obj.pub);
         System.out.println("Value declared in protected : "+obj.callpro());
         System.out.println("Value declared in private : "+obj.callpri());
    }
}