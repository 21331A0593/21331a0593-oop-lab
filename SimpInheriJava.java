class Faculty{
    float salary=100000;
}
class OOPs extends Faculty{
    float bonus=20000;
}
public class SimpInheriJava{
    public static void main(String args[]){
        OOPs obj=new OOPs();
        System.out.println("Total income of OOP faculty is : "+(obj.salary+obj.bonus));
    }
}