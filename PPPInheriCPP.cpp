#include<iostream>
using namespace std;
class Inheritance{
    public:
       int pub=1;
    protected:
       int  prot=2;
    private:
        int priv=3;
    public:
    int callpri(){
        return priv;
    }
};

class Child : public Inheritance{
    public:
    int callpro(){
        return prot;
    }
};
int main(){
    Child obj;
    cout<<"Value declared in public : "<<obj.pub<<endl;
    cout<<"Value declared in protected : "<<obj.callpro()<<endl;
    cout<<"Value declared in private : "<<obj.callpri()<<endl;
}