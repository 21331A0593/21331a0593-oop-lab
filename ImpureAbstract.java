abstract class Abstract{
   abstract void display();
   void print(){
       System.out.println("Impure abstraction");
   }
}
class child extends Abstract{
   void display(){
       System.out.println("display method");
   }
}
public class ImpureAbstract {
   public static void main(String[] args){
       child c=new child();
       c.display();
       c.print();
   }
}