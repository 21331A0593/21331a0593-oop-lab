package mypackage;
public class fib{
   int fib(int i){
       if(i==0){
           return 0;
       }
       else if(i==1){
           return 1;
       }
       else{
           return fib(i-1)+fib(i-2);
       }
   }
   public void fibnum(int n){
       System.out.println(n+" th fibonacci number : "+fib(n));
   }
   public void fibseries(int n){
       int i;
       System.out.print("fibonacci series : ");
       for(i=0;i<=n;i++){
           System.out.print(fib(i)+" ");
       }
   }
}

