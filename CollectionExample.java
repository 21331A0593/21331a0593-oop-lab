import java.util.*;
public class CollectionExample {
    public static void main(String[] args) {

        List<String> fruits = new ArrayList<>();
        fruits.add("Apple");
        fruits.add("Banana");
        fruits.add("Orange");
        System.out.println("List Example: " + fruits);

        Set<Integer> numbers = new HashSet<>();
        numbers.add(10);
        numbers.add(20);
        numbers.add(30);
        numbers.add(20); 
        System.out.println("Set Example: " + numbers);

        Map<String, Integer> ages = new HashMap<>();
        ages.put("John", 25);
        ages.put("Alice", 30);
        ages.put("Bob", 28);
        System.out.println("Map Example: " + ages);


        System.out.println("Iterating over the list:");
        for (String fruit : fruits) {
            System.out.println(fruit);
        }

        boolean containsApple = fruits.contains("Apple");
        System.out.println("List contains Apple? " + containsApple);

        fruits.remove("Banana");
        System.out.println("List after removing Banana: " + fruits);
    }
}