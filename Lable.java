import javax.swing.*;
import java.awt.*;
public class Label {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Label Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());
        JLabel label = new JLabel();
        label.setText("Hii everyone");
        label.setForeground(Color.RED);
        label.setBackground(Color.YELLOW);
        label.setOpaque(true);
        label.setPreferredSize(new Dimension(200, 100));
        frame.add(label);
        frame.setSize(300, 200);
        frame.setVisible(true);
    }
}