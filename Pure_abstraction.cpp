#include<iostream>
using namespace std;
class Abstract{
   public:
       virtual void displayMsg()=0;
};
class Derived:public Abstract{
   public:
       void displayMsg(){
           cout<<"virtual method definition"<<endl;
       }
};
int main(){
   Derived d;
   d.displayMsg();
   return 0;
}