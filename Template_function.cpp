#include<iostream>
using namespace std;
class Template{
    public:
        template<typename myDatatype>
        myDatatype add(myDatatype n1,myDatatype n2){
            return (n1+n2);
        }
};
int main(){
    Template T;
    cout<<"Sum of integers : "<<(T.add<int>(4,5))<<endl;
    cout<<"Sum of float numbers : "<<(T.add<float>(2.45,3.5))<<endl;
    return 0;
}
