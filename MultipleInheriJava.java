class A{
    int a=10;
}
class B extends  A{
    int b=20;
}
class C extends A{
    int c=30;
}
class D extends  B, C{
    int d=a+b+c;
    void display(){
        System.out.println(d);
    }
}
class MultipleInheriJava{
    public static void main(String args[]){
      D obj = new D();
      obj.display();
    }
}