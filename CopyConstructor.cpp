#include<iostream>
using namespace std;
class Student
{
	public:
	string collegename;
	int collegecode;
	string Name;
	double sem;
	Student() //default constructor
	{
    	collegename="MVGR";
    	collegecode=33;
	}
	Student(Student &obj) // copy constructor
	{
    	cout<<"name:  ";
    	cin>>Name;
    	cout<<"sem percentage:  ";
    	cin>>sem;
    	collegename=obj.collegename;
    	collegecode=obj.collegecode;
	}
	void display()
	{
    	cout<<"college name: "<<collegename<<endl;
    	cout<<"code : "<<collegecode<<endl;
    	cout<<"Name : "<<Name<<endl;
    	cout<<"sem : "<<sem<<endl;
	}
	~Student()
	{
    	cout<<"DEAD"<<endl;
	}
	Student(string fullname,double semper) //parameter constructor
	{
  	Name=fullname;
  	sem=semper;
	}
};
int main()
{
	Student obj1;
	Student obj2=obj1;
	cout<<"full name: ";
	cout<<obj2.Name<<endl;
	cout<<"sem percentage: ";
	cout<<obj2.sem<<endl;
	obj2.display();

}
