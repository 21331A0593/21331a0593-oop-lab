import java.io.FileInputStream;
import java.io.FileOutputStream;
public class FileIOstream {
    public static void main(String[] args){
        try{
            FileOutputStream fout=new FileOutputStream("ab.txt");
            FileInputStream fin=new FileInputStream("abc.txt");
            int i=0;
            while(i!=-1){
                fout.write((char)(i=fin.read()));
            }
            fin.close();
            fout.close();
            System.out.println("Success.....");
        }catch(Exception e){
            System.out.println(e);
        }
    }
}