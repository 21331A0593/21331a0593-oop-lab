public class ArithmeticExp{
    public static void main(String[] args){
        try{
            System.out.println(10/0);
        }catch(ArithmeticException ex){
            System.out.println("Denominator must be a nonzero value.");
        }
    }
}