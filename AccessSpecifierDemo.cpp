#include<iostream>
using namespace std;
class AccessSpecifierDemo{
    private: int priVar;
    protected: int proVar;
    public: int pubVar;
    public:
    void setVar(int priValue,int proValue, int pubValue){
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    public:
    void getVar(){
        cout<<"Member variables : "<<priVar<<" "<<proVar<<" "<<pubVar<<endl;
    }
};
int main(){
    AccessSpecifierDemo a;
    a.setVar(1,2,3);
    a.getVar();  
    return 0;
}
