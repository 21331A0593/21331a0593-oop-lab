class userException extends Exception{
    userException(String str){
        super(str);
        System.out.println("Exception");
    }
}
public class UserDefinedExp{
    static void valid(String str) throws userException{
        throw new userException(str);
    }
    public static void main(String[] args){
        String s="Hello java programming";
        try{
            valid(s);
        }catch(Exception e){
            System.out.println(e);
        }
    }
}