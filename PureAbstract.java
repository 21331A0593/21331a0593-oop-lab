interface Abstraction{
   public void display();
   public void print();
}
class Abstract implements Abstraction{
   public void display(){
       System.out.println("display method");
   }
   public void print(){
       System.out.println("print method");
   }
}
public class PureAbstract {
   public static void main(String[] args){
       Abstract abs=new Abstract();
       abs.display();
       abs.print();
   }
}