public class Student{ 
    String fullName;
    int rollNum;
    double semPercentage;
    String collegeName;
    int collegeCode;

    Student(String name,int roll,double sem,String clg,int code){
        fullName=name;
        rollNum=roll;
        semPercentage=sem;
        collegeName =clg;
        collegeCode=code;
    }

    void display(){
        System.out.println("Name of student : "+fullName);
        System.out.println("Roll number of student : "+rollNum);
        System.out.println("Sempecentage of student : "+semPercentage);
        System.out.println("College Name : "+collegeName);
        System.out.println("Colleg Code: "+collegeCode);
    }

    

    public static void main(String args[]){
        Student obj=new Student("Ramprasad",21331,99.8,"MVGR",33);
        obj.display();
    }
}