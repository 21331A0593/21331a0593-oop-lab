class Class1{
     int a=10;
}
      //single
class Class2 extends Class1{ 
        int b=20;
        void display(){
             System.out.println("Single Inheritance called  "+a*b);
        }
}
     //Multilevel 
class Class3 extends Class2{
    void display1(){
             System.out.println("Multilevel Inheritance called "+a*b);
    }
}
interface Class4{
        int d=30;
}
interface Class8{
    int s=100;
}
    // Multiple
class Class5 implements Class8,Class4 {
     int e=s*d;
     void display2(){
            System.out.println("Multiple Inheritance called "+e);
    }
}
    //Hybrid
class Class6 extends Class5{
   
        int f=40;
    void display3(){
            System.out.println("Hybrid Inheritance called "+e*f);
    }
}
     //Hirerachical
class Class7 extends Class1{
    
        int g=50;
    void display4(){
            System.out.println("Hirerachical Inheritance called "+a*g);
    }
}
public  class InheriTypesJava{
    public static void main(String args[]){
        Class2 obj1=new Class2();
        obj1.display();
        Class3 obj2=new Class3();
        obj2.display1();
        Class5 obj3=new Class5();
        obj3.display2();
        Class6 obj4=new Class6();
        obj4.display3();
        Class7 obj5=new Class7();
        obj5.display4();
    }
}