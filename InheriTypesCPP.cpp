#include<iostream>
using namespace std;
class Class1{
    public:
     int a=10;
};
      //single
class Class2 : public Class1{ 
    public:
        int b=20;
        void display(){
             cout<<"Single Inheritance called  "<<a*b<<endl;
        }
};
     //Multilevel 
class Class3: public Class2{
    public:
    void display1(){
             cout<<"Multilevel Inheritance called "<<a*b<<endl;
    }
};
class Class4{
    public:
        int d=30;
};
     //Multiple
class Class5:public Class1,public Class4{
    public:
        int e=a*d;
    void display2(){
            cout<<"Multiple Inheritance called "<<e<<endl;
    }
};
    //Hybrid
class Class6:public Class5{
   public:
        int f=40;
    void display3(){
            cout<<"Hybrid Inheritance called "<<e*f<<endl;
    }
};
     //Hirerachical
class Class7:public Class1{
    public:
        int g=50;
    void display4(){
            cout<<"Hirerachical Inheritance called "<<a*g<<endl;
    }
};
int main(){
    Class2 obj1;
    Class3 obj2;
    Class5 obj3;
    Class6 obj4;
    Class7 obj5;
    obj1.display();
    obj2.display1();
    obj3.display2();
    obj4.display3();
    obj5.display4();
};