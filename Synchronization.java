import java.util.Scanner;
class Table{
    synchronized public void mul(int n){
        for(int i=0;i<5;i++){
           
            System.out.println(n+"*"+i+"="+(n*i));
            try{
                Thread.sleep(100);
            }catch(Exception e){
                System.out.println(e);
            }
           }
        }
}
class thread1 extends Thread{
    Table t;
    thread1(Table t){
        this.t=t;
    }
    public void run(){
        Scanner s=new Scanner(System.in);
        System.out.println("Enter a number :");
        int n=s.nextInt();
        t.mul(n);
        s.close();
    }
       
}
public class Synchronization{
    public static void main(String[] args){
        Table t=new Table();
        thread1 t1 =new thread1(t);
        thread1 t2=new thread1(t);
        t1.start();
        t2.start();
    }
}