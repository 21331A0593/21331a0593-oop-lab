class method1{
   void display(){
       System.out.println("parent class invoked");
   }
}
class method2 extends method1{
   void display(){
       System.out.println("child class invoked.");
   }
}
public class Methodoverride {
   public static void main(String []args){
       method2 m=new method2();
       m.display();
   }
}