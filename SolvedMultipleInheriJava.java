interface A{
	int a=10;
}
interface B extends  A{
	int b=20;
}
interface C extends A{
	int c=30;
}
class D implements  B, C{
	int d=a+b+c;
	void display(){
    	System.out.println("Multiple Inheritance sucessfully " +d);
	}
}
class SolvedMultipleInheriJava{
	public static void main(String[] args){
  	D obj = new D();
  	obj.display();
	}
}
