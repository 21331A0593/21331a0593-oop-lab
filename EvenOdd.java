import java.util.Scanner;

public class EvenOdd {

    public static void main(String[] args) {

        Scanner x = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int n = x.nextInt();

        if(n % 2 == 0)
            System.out.println(n + " is even");
        else
            System.out.println(n + " is odd");
    }
}

