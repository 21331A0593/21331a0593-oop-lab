#include<iostream>
using namespace std;
template <class A>A sum(A a,A b){
   A res=a+b;
   return res;
}
template<class T>T mul(T x,T y){
   return x*y;
}
int main(){
   cout<<"Addition of integers :"<<(sum(4,5))<<endl;
   cout<<"Addition of float numbers: "<<(sum(2.56,1.000))<<endl;
   cout<<"Multiplication of integers: "<<(mul(5,7))<<endl;
   return 0;
}
