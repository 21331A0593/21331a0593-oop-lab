#include<iostream>
using namespace std;
class Abstract{
   public:
       void display(){}
       void displayMsg(){
           cout<<" Impure abstraction"<<endl;
       }
};
class Derived: public Abstract{
   public:
       void display(){
           cout<<" display method definition"<<endl;
       }
};
int main(){
   Derived d;
   d.display();
   d.displayMsg();
   return 0;
}
