#include<iostream>
using namespace std;
class Hod{
    public:
    void instruction(){
        cout<<"HOD : Tomorrow No ClassWork"<<endl;
    }
};
class Faculty: public Hod{
    public:
    void follow(){
        cout<<"Faculty : So we take an extra hour"<<endl;
    }
};
int main(){
    Faculty obj;
    obj.instruction();
    obj.follow();
}