#include<iostream>
#include<iomanip>
using namespace std;
int main(){
    float a=1.109087;
    cout<<"**USAGE OF ENDL Manipulator**"<<endl;
    cout<<"HELLO"<<endl;
    cout<<endl;
    cout<<"Hi ram"<<endl;
    cout<<"**USAGE OF ENDS Manipulator**"<<endl;
    cout<<"Ram"<<ends;
    cout<<"Prasad";
    cout<<endl;
    cout<<"**USAGE OF SETW Manipulator**"<<endl;
    cout<<"1234567890"<<endl;
    cout<<setw(10)<<"hii"<<endl;
    cout<<"**USAGE OF SETFILL Manipulator**"<<endl;
    cout<<setfill('*')<<setw(10)<<"hii"<<endl; //by default it takes from riht to left
    cout<<setfill('*')<<left<<setw(10)<<"hii"<<endl;
    cout<<"**USAGE OF SETPRICISION Manipulator"<<endl;
    cout<<setprecision(4)<<a;
}