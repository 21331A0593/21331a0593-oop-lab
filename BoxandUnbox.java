public class BoxandUnbox{
    public static void main(String[] args){
        Integer a=new Integer(10);  //Boxing
        int b=a;    //Unboxing
        System.out.println(" a value :"+a+"\n b value: "+b);
    }
}
