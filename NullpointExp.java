public class NullpointExp {
    public static void main(String []args){
        String s=null;
        try{
            System.out.println(s.length());
        }catch(NullPointerException e){
            System.out.println(" length of s is zero");
        }
    }
}