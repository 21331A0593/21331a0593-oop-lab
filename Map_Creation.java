import java.util.HashMap;
import java.util.Map;

public class Map_Creation {
    public static void main(String[] args) {
        Map<String, Integer> employeSalary = new HashMap<>();

        employeSalary.put("Vikram", 50000);
        employeSalary.put("Surya", 90000);
        employeSalary.put("Krishnan",72000);

        
        int vikramSalary = employeSalary.get("Vikram");
        System.out.println("Vikram's salary: " + vikramSalary);

     
        boolean Surya = employeSalary.containsKey("Surya");
        System.out.println("Surya is in the map? " + Surya);

        
        employeSalary.put("Iyer", 85000);

        
        employeSalary.remove("Vikram");

     
        System.out.println("Employee Salary:");
        for (Map.Entry<String, Integer> entry : employeSalary.entrySet()) {
            String Employee = entry.getKey();
            int salary = entry.getValue();
            System.out.println(Employee + ": " + salary);
        }
    }
}